## Bienvenido a la documentación de text2cfdi

<div style="text-align: right">
<BR><B>
Todo lo que no podemos dar, no posee.<BR>
Andre Gide</B>
<BR>
<BR>
<BR>
</div>

**text2cfdi** es una serie de scripts en [Python][1] que te ayudan a:

* Tener un entorno de pruebas para generar, sellar y timbrar facturas electrónicas
(CFDI) para la legislación mexicana.
* Convertir archivos de texto simple en formatos: TXT, JSON y YAML a CFDI,
sellarlos y timbrarlos.

### Contenido

1. [Requerimientos previos](requerimientos.md)
1. [Certificados](certificados.md)
1. [Generar el XML](generar.md)
1. [Sellar el XML](sellar.md)
1. [Timbrar el XML](timbrar.md)
1. [Validar el XML](validar.md)
1. [Cancelar un XML](cancelar.md)


### Como ayudar

* Usa estos scripts
* Reportando errores
* Generando archivos de ejemplo
* Escribe documentación
* Propon mejoras


Si eres PAC y estas leyendo esto, estaremos encantados de agregar un ejemplo de
integración de tu servicio a este proyecto.


[1]: https://python.org

