## Sellar el XML


Sellar un XML es firmarlo digitalmente para que no pueda ser manipulado y para
que no se pueda decir ***yo no fui***.

Para sellar un documento se usa el parametro ***-s***.

```
python text2cfdi.py -d ~/pruebas/ -s

Sellando el documento 1 de 1: /home/mau/pruebas/salida/cfdi_minimo.xml
    Documento sellado...
```

Si abres el documento, debes de ver:

```
<?xml version='1.0' encoding='utf-8'?>
<cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" NoCertificado="20001000000300022762" TipoCambio="1" Moneda="MXN" TipoDeComprobante="I" LugarExpedicion="06850" SubTotal="1000.00" Total="1160.00" Version="3.3" Fecha="2019-02-08T22:38:45" Certificado="MIIF8DCCA9igAwIBAgIUMjAwMDEwMDAwMDAzMDAwMjI3NjIwDQYJKoZIhvcNAQELBQAwggFmMSAwHgYDVQQDDBdBLkMuIDIgZGUgcHJ1ZWJhcyg0MDk2KTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSkwJwYJKoZIhvcNAQkBFhphc2lzbmV0QHBydWViYXMuc2F0LmdvYi5teDEmMCQGA1UECQwdQXYuIEhpZGFsZ28gNzcsIENvbC4gR3VlcnJlcm8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQRGlzdHJpdG8gRmVkZXJhbDESMBAGA1UEBwwJQ295b2Fjw6FuMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxITAfBgkqhkiG9w0BCQIMElJlc3BvbnNhYmxlOiBBQ0RNQTAeFw0xNjEwMjEyMDQ3NDVaFw0yMDEwMjEyMDQ3NDVaMIHcMSgwJgYDVQQDEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSgwJgYDVQQpEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSgwJgYDVQQKEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSUwIwYDVQQtExxUQ005NzA2MjVNQjEgLyBIRUdUNzYxMDAzNFMyMR4wHAYDVQQFExUgLyBIRUdUNzYxMDAzTURGUk5OMDkxFTATBgNVBAsUDFBydWViYXNfQ0ZESTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKAzCseikZXkayVixEl49XFGn90qY6EsV7qbi7Mf6wJvfoEcM/azuBvagy9KFe//LqInd4A4K/JwbbSiViJcJ1e0PLOJhOwb8l7Hue/nXtm3bPZKL9+Q87PAFB82/CwZ/qN1RKAAB1E8GyQ05yImw71gN7VbI0i+9Ym1LTLotV5vRSIMJHFNwc1dd6Q4y82S/CbZeDQWIacCt/c5AslL0pSv8F6XzdfetGbel3VoifsA3qNE1q/HePua/H1OJupyGO9jKJcOkWEh5pwic31FDVEMyReF2TCqYLPAH5lU525SJoQOouOEGutW2nnOkTt8xOkRd99JfTJvM/3Y9Zb0DVkCAwEAAaMdMBswDAYDVR0TAQH/BAIwADALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQELBQADggIBAA+okCrsYf2Pl6phFwLFuoNvO4zcGPCQsRrl89ZbDDgdThL3iAoi0wbDOl9+EcJiJTEfDdo8sa6c3Y5ubfZ8zog3SdlguL+Fb5Bz7B1sj2hdQFDtvZl5gkE3tdif4OSMhLQIolBsv4746DM7dtOTKcj3HiwO6KbBPqIFxf6B/zy74Gafg4r6DoiSnp12vTh53fDKOjKB7EIX9+MbuWfwnqtg0ZMvknOpYkLCfDJTIXDNhgk6ykwvaaPxilMMdvJSRutWBprKEZS5G26wSLnnIhW6J8Xm79z8nwQYrGt6TfbjCvFN7KbFaV1c6hLv5cXil2kdirf0CpZWvDEI2ZfQKj2UP0As7z7eIl7VnY8lbIg/JNApOimZ+fLgmikHsSfqE94YzjTB3LLIYsacLA8pOWqm/twkUkCFIC7x+WZIyCtlyegzQdv1I+95Qs5/3RKb9J65LPlvMJgPHVPRGSIObDLiskqGINNmaULB3pABqxP9XkSzpPQI4ME9JaczTN9/mAEoypr7DBRP2ZpeJMusIVvc88Ih2LhBeonza7MiP8uBRVMLSfGUu+Antdgk3Az5q/3Qz+4CvEex9vNL24bMXSfM7mK+Yalw6LeKvDW4SMt+JHQ5fp3cBVyUbWglmjjSt2ehYDjR2t+eIuxqyyshy7iJ2QleM0fuHE0L2GB3C8Rw" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" Sello="K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg==">
  <cfdi:Emisor Rfc="TCM970625MB1" RegimenFiscal="601"/>
  <cfdi:Receptor Rfc="BASM740115RW0" UsoCFDI="G01"/>
  <cfdi:Conceptos>
    <cfdi:Concepto ClaveProdServ="60121001" Cantidad="1.0" ClaveUnidad="KGM" Descripcion="Asesoría en desarrollo" ValorUnitario="1000.00" Importe="1000.00">
      <cfdi:Impuestos>
        <cfdi:Traslados>
          <cfdi:Traslado Base="1000.00" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="160.00"/>
        </cfdi:Traslados>
      </cfdi:Impuestos>
    </cfdi:Concepto>
  </cfdi:Conceptos>
  <cfdi:Impuestos TotalImpuestosTrasladados="160.00">
    <cfdi:Traslados>
      <cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="160.00"/>
    </cfdi:Traslados>
  </cfdi:Impuestos>
</cfdi:Comprobante>
```

El script ha agregado tres nuevos atributos al nodo raíz: **certificado**,
**noCertificado** y **sello**. Los dos primeros los obtuvimos en los
requerimientos previos de este proyecto, y el último se ha generado a partir de
la llave privada y del XML.

```
Sello="K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg=="
```


### Obtener sello manualmente

Para generar el sello manualmente, usaremos las herramientas **openssl** y
**xsltproc**.

#### Generar cadena original

La cadena original es una cadena simple de texto, con una estructura bien
definida, conformada por datos del CFDI, separados por un caracter (pipe |).

Un archivo de transformación (XSLT) sirve para convertir un formato de archivo
(XML en este caso) en otro requerido (TXT) de forma automática, rápida y simple.

Dentro de la carpeta **xlst** de este repositorio, encontrarás los archivos de
transformación publicados por el SAT, con algunas leves modificaciones de nuestra
parte para optimizar su desempeño.

Pon atención en las rutas de cada archivo, asegurate de usar las correctas en
tu equipo.

```
xsltproc xslt/cadena.xslt ~/pruebas/salida/cfdi_minimo.xml > cadena.txt
```

El archivo generado debe verse como:

```
cat cadena.txt

||3.3|2019-02-08T22:38:45|20001000000300022762|1000.00|MXN|1|1160.00|I|06850|TCM970625MB1|601|BASM740115RW0|G01|60121001|1.0|KGM|Asesoría en desarrollo|1000.00|1000.00|1000.00|002|Tasa|0.160000|160.00|002|Tasa|0.160000|160.00|160.00||

```

Conozco, algunos masoquistas que generan esta cadena **a mano**.


#### Firmar la cadena original

Con la llave privada (KEY) se firma esta cadena. Como usamos el PEM encriptado
debe de pedirte la contraseña respectiva.

```
openssl dgst -sha256 -sign cert/finkok.pem.enc cadena.txt > sello.enc

Enter pass phrase for cert/finkok.pem.enc:
```

El archivo generado **sello.enc**, es el sello en formato binario.


#### Convertir a base64

El sello binario se convierte a formato **base64** para poder integrarlo al CFDI.

```
openssl enc -base64 -A -in sello.enc > sello.txt
```

Ahora si, podemos verlo y comprobar que es igual al generado al inicio.

```
cat sello.txt

K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg==
```

#### Generar el sello en un solo paso

```
xsltproc xslt/cadena.xslt ~/pruebas/salida/cfdi_minimo.xml | openssl dgst -sha256 -sign cert/finkok.pem.enc | openssl enc -base64 -A

Enter pass phrase for cert/finkok.pem.enc:

K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg==
```

**IMPORTANTE: La fecha y hora forma parte de la cadena original y por tanto del
sello, por eso, NO obtendrás el mismo sello cuando tu hagas tus pruebas**
