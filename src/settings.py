#!/usr/bin/env python
# coding: utf-8

import sys
import logbook
import os
from logbook import Logger, StreamHandler, RotatingFileHandler
logbook.set_datetime_format("local")

from conf import DEBUG, TOKEN

# ~ PACs probados finkok, ecodex
PAC = 'finkok'

PY2 = sys.version_info[0] == 2

#~ Establece la ruta al archivo LOG de registros
LOG_PATH = '/tmp/text2cfdi.log'
LOG_NAME = 'CFDI'
LOG_LEVEL = 'INFO'
if DEBUG:
    LOG_LEVEL = 'DEBUG'

format_string = '[{record.time:%d-%b-%Y %H:%M:%S}] ' \
    '{record.level_name}: ' \
    '{record.channel}: ' \
    '{record.message}'

RotatingFileHandler(
    LOG_PATH,
    backup_count=10,
    max_size=1073741824,
    level=LOG_LEVEL,
    format_string=format_string).push_application()

StreamHandler(
    sys.stdout,
    level=LOG_LEVEL,
    format_string=format_string).push_application()

log = Logger(LOG_NAME)

#~ Aumenta el tiempo de espera en segundos, solo si tienes una conexión muy
#~ lenta o inestable
TIMEOUT = 10

NAME_XSLT = 'cadena_3.3_1.2.xslt'
PATH_PROYECT = os.path.dirname(__file__)
PATH_CERT = os.path.join(PATH_PROYECT, 'cert',)
PATH_XSLT = os.path.join(PATH_PROYECT, 'xslt', NAME_XSLT)

#~ Directorios de trabajo
FOLDERS = {
    'ENTRADA': 'entrada',
    'SALIDA': 'salida',
    'TIMBRADOS': 'timbrados',
}

#~ Nombre del certificado: IMPORTANTE, establece SOLO el nombre, TODOS los
#~ archivos deben de TENER el MISMO NOMBRE, se requiere al menos el CER y PEM
CERT_NAME = 'certificado.{}'
if DEBUG:
    CERT_NAME = PAC + '.{}'

# ~ Establece en True para borrar los archivos origen, recomendado en producción
DELETE_SOURCE = False
