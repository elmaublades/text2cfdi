text2cfdi es un conjunto de scripts, para aprender como generar, sellar y
timbrar facturas electrónicas (CFDI) bajo la legislación mexicana.

[Documentación](https://doc.elmau.net/text2cfdi)
